use serenity::prelude::*;
use serenity::model::channel::Message;
use serenity::client::bridge::gateway::ShardManager;
use serenity::framework::standard::{
    macros::command,
    CommandResult
};
use crate::utils::{
    functions::prisbot_embed,
    tymap::ShardContainer
};

#[command("logout")]
#[aliases("exit", "terminate", "stop", "shutdown")]
#[owners_only]
/// Shutdown the bot.
///
/// NOTE: This will not allow currently running commands to finish executing. Be
/// *very careful* with this!
pub async fn sudo_logout(ctx: &Context, msg: &Message) -> CommandResult {
    let bot_user = ctx.cache.current_user().await;
    msg.channel_id.send_message(&ctx, |m| {
        m.embed(|e| {
            prisbot_embed(e).description(format!("__*Shutting down {}...*__", bot_user.tag()))
        })
    }).await;
    warn!("Shutdown in progress...");
    let data = ctx.data.read().await;
    let mut manager = data.get::<ShardContainer>().unwrap().lock().await;
    manager.shutdown_all().await;
    Ok(())
}
