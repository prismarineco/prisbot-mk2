use serenity::framework::standard::{
    macros::command,
    CommandResult
};
use slog_unwrap::ResultExt;
use serenity::prelude::*;
use serenity::model::channel::Message;

/// See statistics about the bot's operation. Version, compiler, uptime,
/// deployment, etc.
#[command("top")]
#[aliases("info", "stat")]
#[owners_only]
/// List various pieces of system info, such as the target the bot was compiled for, the `rustc`
/// version used, a short commit hash, and its runtime.
pub async fn sudo_top(ctx: &Context, msg: &Message) -> CommandResult {

    let bot_ver = env!("CARGO_PKG_VERSION");
    let compiler_ver = env!("PRISBOT_COMPILER_VERSION");
    let target = env!("PRISBOT_HOST_TARGET");
    let commit_info = env!("PRISBOT_COMMIT_INFO");
    let (days, hours, mins, secs) = {
        // First, get the duration
        let data = ctx.data.read().await;
        let start = data.get::<crate::utils::StartTime>().unwrap();
        let duration: std::time::Duration = std::time::Instant::now() - start.0;

        // Next, split that up into days, hours, minutes, and seconds.
        let days = duration.as_secs() / 86400;
        let hours = (duration.as_secs() % 86400) / 3600;
        let minutes = ((duration.as_secs() % 86400) % 3600) / 60;
        let seconds = ((duration.as_secs() % 86400) % 3600) % 60;

        // And now, return them as a tuple.
        (days, hours, minutes, seconds)
    };

    let mut runtime_string = String::new();

    if days > 0 {
        runtime_string += format!("{} days, ", days).as_str();
    }
    if hours > 0 {
        runtime_string += format!("{} hours, ", hours).as_str();
    }
    if mins > 0 {
        runtime_string += format!("{} minutes, ", mins).as_str();
    }
    if secs > 0 {
        runtime_string += format!("{} seconds", secs).as_str();
    }

    msg.channel_id.send_message(&ctx, |m| m.embed(|e| {
        crate::utils::functions::prisbot_embed(e)
            .title("System Information")
            .fields(vec![
                ("Crate Version", bot_ver, true),
                ("Rust Compiler Version", compiler_ver, true),
                ("Target Triple", target, true),
                ("Commit ID", commit_info, true),
                ("Time Running", &runtime_string, true)
            ])
    })).await.unwrap_or_log();

    Ok(())
}
