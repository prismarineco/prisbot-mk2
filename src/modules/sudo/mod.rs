// Copyright (C) 2020 The Prismarine Company
use serenity::framework::standard::macros::group;

mod logout;
use logout::SUDO_LOGOUT_COMMAND;

mod top;
use top::SUDO_TOP_COMMAND;

#[group]
#[commands(sudo_logout, sudo_top)]
#[prefix("sudo")]
pub struct SudoMod;
