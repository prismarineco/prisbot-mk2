#![allow(warnings)]
//! The Module System is contained within this directory.
//!
//! Each module should be broken up into the following structure, with each
//! module recieving its own folder, and each file containing *exactly* one
//! command.
//! ``` <module_folder>
//!         |- mod.rs
//!         |- command_1.rs
//!         |- command_2.rs
//! ```
// Copyright (C) 2020 The Prismarine Company
use serenity::{
    prelude::*,
    framework::standard::{
        StandardFramework,
        DispatchError,
        macros::hook,
    },
    model::{
        id::UserId,
        channel::Message
    }
};

/// Miscelaneous commands. Anything that doesn't fit cleanly into the other
/// groups goes here.
mod misc;

/// Player commands. Anything that relates to player profile modification,
/// creation, or deletion goes here.
mod player;

/// Server Configuration. Things like setting prefixes, blacklisting channels
/// (or users) from using the bot, etc. go here.
mod server_cfg;

/// DEVELOPER-ONLY administration commands. These should only ever be used or
/// usable by the bot's developers.
mod sudo;

/// Team Profiler commands. Team search, modification, and creation functions go
/// here.
mod team;

/// WPCR: Prismarine Co. Radio. Subscribing to specific tags, reporting
/// broadcasts, broadcaster verification, and more go here.
mod wpcr;

/// Competitive Testing Initiative commands. Constructing (and deconstructing)
/// leagues, finding and reporting matches, assigning people to be match
/// observers, etc. goes here.
mod cti;

/// Rotation module. Commands for seeing future Turf, Ranked, and League
/// Rotations belong here.
mod rotation;

#[hook]
async fn dispatch_failure(_ctx: &Context, _msg: &Message, _err: DispatchError) {}

pub fn gen_framework(conf: &crate::conf::Config) -> StandardFramework {

    let framework = StandardFramework::new()
        // Configure the framework
        .configure(|c| c
                   .prefix(&conf.discord.prefix)
                   .owners({
                       let mut owners = std::collections::HashSet::new();
                       for id in &conf.discord.owners {
                           owners.insert(UserId(*id));
                       }
                       owners
                   }))
        // Add hooks
        .on_dispatch_error(dispatch_failure)
        // Register command groups
        .group(&sudo::SUDOMOD_GROUP);

    framework
}
