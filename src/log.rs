use slog::Drain;

/// Set up the bot's main logger. Returns a guard that should be held until program termination.
pub fn setup() -> slog_scope::GlobalLoggerGuard {

    // File Log Output
    let file_log = {
        let log_path = option_env!("PRISBOT_LOG_FILE")
            .unwrap_or("logs/prisbot.log");
        let file = std::fs::OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(log_path)
            .unwrap_or_else(|_|
                            panic!("Failed to open log file `{}`", log_path));

        let decorator = slog_term::PlainDecorator::new(file);
        let drain = slog_term::FullFormat::new(decorator).build().fuse();
        slog_async::Async::new(drain).build().fuse()
    };

    // Terminal Log Output
    let term_log = {
        let decorator = slog_term::TermDecorator::new().build();
        let drain = slog_term::FullFormat::new(decorator).build().fuse();
        let drain = slog_async::Async::new(drain)
            .build()
            .fuse();

        if cfg!(debug_assertions) {
            drain.filter_level(slog::Level::Info)
        } else {
            drain.filter_level(slog::Level::Error)
        }
    };

    let logger = slog::Logger::root(
        slog::Duplicate::new(file_log, term_log).fuse(),
        slog::o!()
    );

    slog_scope::set_global_logger(logger)
}
