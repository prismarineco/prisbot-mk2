// Copyright (C) 2020 The Prismarine Company.

// Tokio Runtime
extern crate tokio;

// Logging Framework
extern crate slog;
extern crate slog_async;
extern crate slog_term;
extern crate slog_unwrap;
#[macro_use]
extern crate slog_scope;

// Error Handling
extern crate anyhow;
extern crate thiserror;

// Database Library
extern crate sqlx;

// Configuration Parsing
extern crate toml;
extern crate serde;

// Discord API Library (the core lib of the bot.)
extern crate serenity;

/// Log setup module. Logging configuration goes here, and the main logger will
/// be retrieved via `log::setup()`.
mod log;

/// Configuration module. Anything related to the bot's config options goes
/// here.
mod conf;

/// The Command Module. All the commands that the bot has available goes here.
/// *In general*, only code relating to commands should be here.
mod modules;

/// Various utilities for the bot's development and usage.
mod utils;

use slog_unwrap::ResultExt;

#[tokio::main]
async fn main() {
    // Set up the global logger.
    let __log__ = log::setup();
    info!("Beginning initialization";
          "bot-version" => env!("CARGO_PKG_VERSION"));

    // Read the config.
    let config = conf::read_config();
    info!("Configuration parsed");

    // Verify the database's operation and retrieve a pool.
    let db_pool = sqlx::MySqlPool::new(&config.database.url)
        .await
        .unwrap_or_log();

    {
        let version = sqlx::query!("SELECT VERSION() as version")
            .fetch_one(&db_pool)
            .await
            .unwrap_or_log();
        info!("Database connected";"version" => version.version);
    }

    let db = utils::Database::with_mysql(db_pool);

    // Setup the client
    let mut client = serenity::Client::new(&config.discord.token)
        .framework(modules::gen_framework(&config))
        .event_handler(utils::EventLogger)
        .await.unwrap_or_log();

    // Get an uptime tracker
    let timer = utils::StartTime(std::time::Instant::now());

    // Insert needed data into the typemap
    {
        let mut data = client.data.write().await;
        data.insert::<utils::tymap::ShardContainer>(client.shard_manager.clone());
        data.insert::<utils::StartTime>(timer);
        data.insert::<utils::Database>(db);
    }

    // And now, connect to the gateway and start recieving events!
    client.start_autosharded().await.expect_or_log("Client failure occured!");
}
