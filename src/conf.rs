// Copyright (C) 2020 by The Prismarine Company
use slog_unwrap::ResultExt;
use std::io::Read;

#[derive(serde::Deserialize)]
pub struct TopLevel {
    conf: Config
}

#[derive(serde::Deserialize)]
pub struct Config {
    pub discord: DiscordConf,
    pub database: DatabaseConf
}

#[derive(serde::Deserialize)]
pub struct DiscordConf {
    /// The token for the bot. The most important thing needed here.
    pub token: String,

    /// A vector of user IDs that are allowed to use the bot's admin
    /// functionality.
    pub owners: Vec<u64>,

    /// The default prefix for the bot.
    pub prefix: String,
}

#[derive(serde::Deserialize)]
pub struct DatabaseConf {
    /// The url to connect to the database.
    pub url: String
}

pub fn read_config() -> Config {
    let mut conf = String::new();
    std::fs::File::open("prisbot.toml")
        .expect_or_log(
            "Expected configuration file named `prisbot.toml` at project root")
        .read_to_string(&mut conf)
        .unwrap_or_log();

    let conf: TopLevel = toml::from_str(conf.as_str()).unwrap_or_log();
    conf.conf
}
