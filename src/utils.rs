use serenity::prelude::{TypeMapKey, EventHandler, Context};
use serenity::model::gateway::Ready;
use anyhow::Result as AnyResult;

/// Indicates that the item implementing this trait can be updated into whatever
/// backend is used by the bot.
///
/// NOTE: Only call this from within the backend (e.g. flushing player updates
/// from a team update). If you want to update something that implements this
/// trait from the discord frontend, it's preferred that you use
/// [`Database::flush`] instead and bring this trait into scope.
#[serenity::async_trait]
pub trait Updatable {
    async fn update(self, conn: impl sqlx::Executor) -> AnyResult<()>;
}

/// Indicates an object that can be fetched from the database.
///
/// This trait is used in conjunction with the `Database` struct, meant to
/// provide a unified interface for fetching objects from database.
///
/// NOTE: Only call this from within the backend (i.e. flushing player updates
/// from a team update). If you want to fetch something for use within a command
/// (such as getting a player for `player update`), prefer [`Database::get`]
/// instead and bring this trait into scope.
#[serenity::async_trait]
pub trait Fetchable: Sized {

    /// The key that's used to refer to an object in the database. Generally,
    /// this is its `PRIMARY KEY`, although it doesn't have to be.
    type Key;

    /// The function used to fetch the object from the database.
    async fn fetch(
        key: Self::Key,
        conn: impl sqlx::Executor
    ) -> AnyResult<Option<Self>>;
}

/// A database abstraction.
///
/// This is used to abstract away the details of fetching and updating data from
/// the database. An example would be updating a player:
///
/// ```ignore
///
/// let id = 1; // Pretend we got some kind of user snowflake
/// let db = get_db!(); // A convenience macro, saves the effort of writing the
///                     // code to get the `Database` struct from serenity's
///                     // typemap.
/// let player = db.get::<Player>(id); // Fetch the player from the database!
///
/// /* some modifications later... */
///
/// db.flush(player); // And flush whatever updates we've made.
/// ```
pub struct Database {
    // For some reason, despite being used in functions,
    // this gets marked as unread for some reason. Maybe I should implement
    // `DerefMut` on it, but I think the lint is enough. - A. Bluefall
    #[allow(dead_code)]
    backend: sqlx::MySqlPool
}

impl TypeMapKey for Database {
    type Value = Self;
}

impl Database {
    /// Wrap a MySQL database instance.
    pub fn with_mysql(backend: sqlx::MySqlPool) -> Self {
        Database {backend}
    }

    /// Flushes all the updates made to an [`Updatable`] struct. The update call
    /// is wrapped in a transaction, to assure that a partial update is not
    /// saved in case of an error.
    ///
    /// NOTE: This should only really be called from the discord frontend. If
    /// you want to propagate updates down (such as when updating players
    /// associated with a team), prefer using [`Updatable::update`] instead.
    pub async fn flush<T: Updatable>(&self, val: T) -> AnyResult<()> {
        let mut transaction = self.backend.begin().await?;
        val.update(&mut transaction).await?;
        transaction.commit().await?;
        Ok(())
    }

    /// Fetches a [`Fetchable`] object from the database.
    ///
    /// NOTE: This should only really be called from the discord frontend. If
    /// you want to fetch data from the bot's buisness logic (like when getting
    /// item data), prefer [`Fetchable::fetch`] instead.
    pub async fn get<T: Fetchable>(&self, key: T::Key) -> AnyResult<Option<T>> {
        Ok(T::fetch(key, &self.backend).await?)
    }
}


/// What it says on the tin. This is to log any important events to the bot.
pub struct EventLogger;
#[serenity::async_trait]
impl EventHandler for EventLogger {
    async fn ready(&self, _: Context, ready_info: Ready) {
        info!("Client initialized! {} is online!", ready_info.user.tag());

}
}

/// Various convenience functions, made to reuse commonly used code around the
/// bot.
pub mod functions {
    pub fn prisbot_embed(em: &mut serenity::builder::CreateEmbed)
                         -> &mut serenity::builder::CreateEmbed {
        em.author(|a| a.icon_url(env!("PRISBOT_ICON_URL"))
                         .name(env!("PRISBOT_NAME")))
                         .color((0, 0, 0))
    }
}

/// A means of tracking the bot's runtime.
pub struct StartTime(pub std::time::Instant);

/* SERENITY TYPE MAP KEYS */
// I'm sticking these here because they really belong nowhere else. They're
// essentially just boilerplate. If anything needs to be stored within
// `serenity::Client`'s typemap, it's TypeMapKey impl will go here.
#[doc(hidden)]
pub mod tymap {
    use std::sync::Arc;
    use tokio::sync::Mutex;
    use serenity::client::bridge::gateway::ShardManager;
    pub struct ShardContainer;
    impl super::TypeMapKey for ShardContainer {
        type Value = Arc<Mutex<ShardManager>>;
    }
    impl super::TypeMapKey for super::StartTime {
        type Value = Self;
    }
}
