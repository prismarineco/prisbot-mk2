<p>
<img src="./prisco_bot_blackwhite_cmd_banner.svg" width="100%" height="250">
</p>

Here you will find the documentation for every command. This includes, but is not limited to:

- Descriptions
- Examples
- Required Permissions
- Valid and Invalid inputs
- Descriptions of any known edge cases
- etc.

If the documentation for any command seems incomplete, please notify the Prismarine Company as soon as possible so that the issue may be corrected, or create a Merge Request with a fix.

This documentation is also accessible via the bot's `help` command.

# Table of Contents

- [Player Profiler](#player)
- [Team Profiler](#team)
- [Rotation Info](#rotation)
- [Prismarine Co. Radio](#wpcr)
- [Competitive Testing Initiative](#cti)
- [Server Configuration](#cfg)
- [Misc.](#misc)
- [System Administration](#sudo)

<a name="player">
# The Player Profiler // `player`
</a>
*Unimplemented. Come back later and perhaps you'll see something new!*

<a name="team">
# The Team Profiler // `team`
</a>
*Unimplemented. Come back later and perhaps you'll see something new!*
<a name="rotation">
# Rotation Data // `rotation`
</a>
*Unimplemented. Come back later and perhaps you'll see something new!*
<a name="wpcr">
# Prismarine Co. Radio // `wpcr`
</a>
*Unimplemented. Come back later and perhaps you'll see something new!*
<a name="cti">
# Competitive Testing Initiative // `cti`
</a>
*Unimplemented. Come back later and perhaps you'll see something new!*
<a name="cfg">
# Server Configuration // `cfg`
</a>
*Unimplemented. Come back later and perhaps you'll see something new!*
<a name="misc">
# Miscelaneous Commands
</a>
*Unimplemented. Come back later and perhaps you'll see something new!*
<a name="sudo">
# System Administration Commands // `sudo`
</a>
___NOTE: These commands are not to be used by the general public. They are only intended to be used for development and administration purposes!___

- `sudo logout`
Shutdown the bot.
NOTE: This will not allow currently executing commands to finish. Be *very careful* with this!

- `sudo top`
List various pieces of system info, such as the target the bot was compiled for, the `rustc` version used, a short commit hash, and its runtime.

