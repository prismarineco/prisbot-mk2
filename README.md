<p>
<img src="./prisco_bot_blackwhite_banner.svg" width="100%" height="250">
</p>


We here at *The Prismarine Company* are proud to present to you with *Project Prismarine*, the definining product of our corporation.

We present such features as:

### The Player Profiler
A method of showing off your competitive prowess, easily sharing your friend code, telling people a little bit about yourself, all in a convienient little module.

### The Team Profiler
A unique feature to *Project Prismarine*, the Team Profiler allows you to show off your roster, tournament results, and a description about your team, all in one place.

### WPCR // Prismarine Co. Radio
Tune your dials (read: subscribe your channels) to WPCR in order to hear the latest in not just news about *Project Prismarine*, but various tournaments going on as well. For teams and free agents, it's a one-stop shop to the competitive world. For tournament organizers, it's an easy way to get your message out to the community.

### Competitive Testing Initiative
A simple and easy way to host and organize tournaments within servers. For tournament organizers, you can set up tournaments, provide easy commands for your users to see various types of info (such as rulesets, links, etc), sign up and check in, report results and disputes, and when all is set and done, generate a final message containing the results. The CTI will also be able to integrate with the Team Profiler, automatically registering wins achieved within the CTI system.

... and possibly more as time goes on.

#### "How do I get started?"
Check out the [quick start](docs/QUICK_START.md) file.
#### "Where are all the commands?"
In two places:
- The `help` command, or
- In the [commands](docs/COMMANDS.md) file.

If any command is unimplemented, contact The Prismarine Company. Speaking of which...
#### "How do I contact the Prismarine Company?"
Easily. Our discord server is right [here](https://discord.gg/eSRnUa). Alternatively, you may contact the main developer directly via [email](mailto:sbisonol@gmail.com) or his discord ID: `Alex Bluefall\#3305`.
#### "How do I configure the bot so I can run it locally?"
There are a few things you'll need to run the bot:

- A Discord bot token, which can be acquired via the [Discord Developer Portal](https://discord.com/developers).
- Your user ID, as well as the ID of any other developers you may be working
  with. [Read here](https://support.discord.com/hc/en-us/articles/206346498-Where-can-I-find-my-User-Server-Message-ID-) in order to find out how to get that.
- A suitable prefix.
- A running MySQL database, as well as its connection URL.

All these items go into a configuration file named `prisbot.toml`, and that file
should be placed at the base directory of the project.

Here's a sample config that you can use.

```toml
[conf.discord]
token = "<YOUR_TOKEN_HERE>" # token from the dev portal
owners = [ 490650609641324544 , 304406232582717440 ] # your user ID
prefix = "prx." # the default prefix you want
icon_url = "https://cdn.discordapp.com/attachments/704080492487508038/726328139160223744/prisco_bot_blackwhite.png" # An avatar URL, to be used in embeds
name = "Project Prismarine" # The name of the bot, also to be used in embeds.

[conf.database]
url = "mysql://user:password@127.0.0.1/project_prismarine" # the url needed to connect to your database
```
#### "What's the license?"
```
    Copyright (C) 2020 The Prismarine Company.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
