// Copyright (C) 2020 by The Prismarine Company
extern crate toml;
extern crate serde;
extern crate rustc_version;
use std::io::Read;

// I know this is a decent bit of code duplication *just* to extract the
// database url from the config file, but if anyone else has a better idea, @ me
// in the prisco discord, or make a pull request.

#[derive(serde::Deserialize)]
pub struct TopLevel {
    conf: Config
}
#[derive(serde::Deserialize)]
pub struct Config {
    pub discord: DiscordConf,
    pub database: DatabaseConf
}
#[derive(serde::Deserialize)]
pub struct DiscordConf {
    icon_url: String,
    name: String,
}
#[derive(serde::Deserialize)]
pub struct DatabaseConf {
    pub url: String
}

fn main() {
    println!("cargo:rerun-if-changed=prisbot.toml");
    let mut conf = String::new();
    std::fs::File::open("prisbot.toml")
        .expect(
            "Expected configuration file named `prisbot.toml` at project root")
        .read_to_string(&mut conf).unwrap();
    let conf: TopLevel = toml::from_str(conf.as_str()).unwrap();

    println!("cargo:rustc-env=DATABASE_URL={}", conf.conf.database.url);
    println!("cargo:rustc-env=PRISBOT_NAME={}", conf.conf.discord.name);
    println!("cargo:rustc-env=PRISBOT_ICON_URL={}", conf.conf.discord.icon_url);

    // Get info about the compiler and pass it onto the bot
    let rustc_version_data = rustc_version::version_meta().unwrap();
    println!("cargo:rustc-env=PRISBOT_COMPILER_VERSION={}",
             rustc_version_data.semver);
    println!("cargo:rustc-env=PRISBOT_HOST_TARGET={}", rustc_version_data.host);

    // Get revision data
    let git_rev = std::process::Command::new("git")
        .args(&["rev-parse", "--short", "HEAD"])
        .output()
        .unwrap();

    let git_rev = String::from_utf8(git_rev.stdout)
        .unwrap_or_else(|_| "<UNKNOWN>".to_string());
    println!("cargo:rustc-env=PRISBOT_COMMIT_INFO={}", git_rev);

}
